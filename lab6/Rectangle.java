
public class Rectangle {
	
	int sideA;
	int sideB;
	
	Point topLeft;
	
	
	public Rectangle(int a, int b, Point p) {
		sideA=a;
		sideB=b;
		topLeft=p;
	}
	public int area(){
		
		return sideA*sideB;
	}
	
	public int perimeter() {
		return 2*sideA+2*sideB;
	}
	public Point[]  corners() {
		Point bottomLeft=new Point (topLeft.xCoord,topLeft.yCoord+sideB);
		Point topRight=new Point (topLeft.xCoord+sideA,topLeft.yCoord);
		Point bottomRight=new Point(topLeft.xCoord+sideA,topLeft.yCoord+sideB);
		Point[] arr={topLeft,topRight,bottomLeft,bottomRight};
		
		
		return arr;
	}
	

}
